"use strict";
(function($){
	$('.tabs .tab').on({
		'mouseenter':function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var obj = $(this);
			var id = obj.attr('data-info');
			var info = $(id);
			var parent = info.parents('.tabs');
			parent.find('.show, .hover').removeClass('show hover');
			parent.find('.active').removeClass('active');
			if(!info.hasClass('hover') || !info.hasClass('show')){
				info.addClass('hover');
				obj.addClass('active');
			}
		},
		'click':function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var obj = $(this);
			var id = obj.attr('data-info');
			var info = $(id);
			var parent = info.parents('.tabs');
			parent.find('.show, .hover').removeClass('show hover');
			parent.find('.active').removeClass('active');
			if(!info.hasClass('show')){
				info.addClass('show');
				obj.addClass('active');
			}
		}
	});
	//jQuery for page scrolling feature - requires jQuery Easing plugin
	$(function() {
	    $('a.page-scroll').bind('click', function(event) {
	        var $anchor = $(this);
	        $('html, body').stop().animate({
	            scrollTop: $($anchor.attr('href')).offset().top
	        }, 1500, 'easeInOutExpo');
	        event.preventDefault();
	    });
	});

	$(".watch").click(function() {

		 $("#video")[0].src += "&autoplay=1";
         ev.preventDefault();

	});
	
}(jQuery));
